# CHOUCAS PROJECT

This repo was created to test technologies, snippets of code,... Its primary purpose is not to break the main git with tests.

## Install project

### Requirements 

Is required to install:
* PosgreSQL
* Postgis
* Python3
* Pip
* Virtualenv

### Installation

#### 1. Project init
```bash
$ git clone git@gricad-gitlab.univ-grenoble-alpes.fr:Projets-INFO4/19-20/8/draft.git
$ cd Draft/choucas6
$ source bin/activate
$ cd choucas
$ pip install -r requirements.txt 
```

If is necessay, edit the setting file (`choucas/settings.py`) for database connection.

#### 2. Database creation

```bash
$ python manage.py makemigrations
$ python manage.py migrate
```

#### 3. Load data

```bash
$ python manage.py loaddata...
$ python manage.py makemigrations
$ python manage.py migrate
```

#### 4. Run server

```bash
$ python manage.py runserver
```




	
